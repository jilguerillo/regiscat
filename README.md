<div align="center">

<p><img src="https://gitlab.com/jilguerillo/regiscat/uploads/69fa5fd664b61ecdecf3186761fc9362/icon_ic.png" width="200"></p>

# RegisCat

### RegisCat es un mapa de cámaras de vigilancia en Chile. Una aplicación de cartografía para Android.

</div>

----
<div>
<h3>About</h3>

Este mapa está construido en base a OpenStreetMap.
Esta aplicación permite el seguimiento de las cámaras de vigilancia en Chile (por ahora sólo Chile).

<a href="https://f-droid.org/es/packages/com.example.regis_cat/">Descarga la aplicación desde
Fdroid</a>

<ul>
<li>Seguridad y protección de la privacidad</li> 
<li>No rastrea</li>
<li>No GoogleMap</li>
</ul>

<div>
<img src="https://gitlab.com/jilguerillo/registro-espionaje/uploads/a99acb073db1ee6a6787d9c8aaaceac1/screenshot1.jpg" height="400" width="200" >

<img src="https://gitlab.com/jilguerillo/registro-espionaje/uploads/8ab965e9cb94639e16c09585ca60dcc8/screenshot5.jpg" height="400" width="200" >



<img src="https://gitlab.com/jilguerillo/registro-espionaje/uploads/81f83af2d60185f9742f3383eef77964/screenshot4.jpg" height="400" width="200">



<img src="https://gitlab.com/jilguerillo/registro-espionaje/uploads/0bd3b27956e1d35187fddc9c674d00fa/screenshot3.jpg" height="400" width="200">



<img src="https://gitlab.com/jilguerillo/registro-espionaje/uploads/242a761ebcf6a50346d732637f56f456/screenshot2.jpg" height="400" width="200">
</div>

</div>

----
<div>
<h3>Permisos para Android</h3>

RegisCat no necesita permisos excepto Conexión a Internet, Localización (opcional) y
Almacenamiento (opcional).

*Esta aplicación está en su fase BETA, todas las funciones pueden no ser 100% operativas.*
</div>
<div align="center">
<p><img src="https://gitlab.com/jilguerillo/registro-espionaje/uploads/f22b27ecc398c1408c84b0dd6034c5f7/icon_dw.png" width="200"></p>
</div>

----
<div>
<h3>Licencia</h3>

    RegisCat
    Copyright (C) 2022  Jilguerillo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

</div>

