package com.example.regis_cat.Card

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.Search
import androidx.compose.material.icons.filled.Share
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.regis_cat.R

@Composable
fun CardRecoFace(){
    Card (
        modifier = Modifier
            .padding(8.dp)
            .clickable { }
            .fillMaxWidth(),
        elevation = 8.dp,
        shape = RoundedCornerShape(8.dp)
    ) {
        Column(){
            Image(painterResource(id = R.drawable.reco_f),
                contentDescription = "Camara de vigilancia con reconocimiento facial")
            Column(modifier = Modifier.padding(16.dp)) {
                // requiere modificar style
                Text("Agregar descripción camara reconocimiento facial",
                    style = MaterialTheme.typography.body1,
                    modifier = Modifier.padding(bottom = 8.dp))
                Text(text = "Agregar la descripción general desde string",
                    style = MaterialTheme.typography.body2)
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceBetween
                ){
                    Box(){
                        Row {
                            TextButton(onClick = { /*TODO*/ }) {
                                Text("Camaras")
                            }
                            TextButton(onClick = { /*TODO*/ }) {
                                Text("Tipo")
                            }
                        }

                    }
                    Box(){
                        Row {
                            IconButton(onClick = { /*TODO*/ }) {
                                IconButton(onClick = { /*TODO*/ }) {
                                    Icon(imageVector = Icons.Filled.Search,
                                        contentDescription = "Boton favorito")
                                }
                            }
                            IconButton(onClick = { /*TODO*/ }) {
                                IconButton(onClick = { /*TODO*/ }) {
                                    Icon(imageVector = Icons.Filled.Share,
                                        contentDescription = "Boton favorito")
                                }
                            }
                        }
                    }
                }

            }

        }
    }
}

@Preview(showBackground = true)
@Composable
fun PreviewCardRecoFace(){
    MaterialTheme {
        CardRecoFace()
    }
}