package com.example.regis_cat.Fragments;

import android.content.res.Resources;
import android.os.Bundle;

import androidx.core.text.HtmlCompat;
import androidx.fragment.app.Fragment;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.regis_cat.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PanovuFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PanovuFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public PanovuFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_panovu, container, false);

        Resources resources = getResources();
        String texto = resources.getString(R.string.large_text_panovu);
        CharSequence textoInterpretado = HtmlCompat.fromHtml(texto, Html.FROM_HTML_MODE_LEGACY);

        TextView textView = (TextView) root.findViewById(R.id.txt_scrollable_panovu);
        textView.setText(textoInterpretado);
        return root;
    }
}