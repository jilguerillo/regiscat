package com.example.regis_cat.Fragments;

import android.content.res.Resources;
import android.os.Bundle;

import androidx.core.text.HtmlCompat;
import androidx.fragment.app.Fragment;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.regis_cat.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ReconocimientoFacialFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ReconocimientoFacialFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public ReconocimientoFacialFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ReconocimientoFacialFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ReconocimientoFacialFragment newInstance(String param1, String param2) {
        ReconocimientoFacialFragment fragment = new ReconocimientoFacialFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root =  inflater.inflate(R.layout.fragment_reconocimiento_facial, container, false);

        Resources resources = getResources();
        String texto = resources.getString(R.string.large_text_rf);
        CharSequence textoInterpretado = HtmlCompat.fromHtml(texto, Html.FROM_HTML_MODE_LEGACY);

        TextView textView = (TextView) root.findViewById(R.id.txt_scrollable_rf);
        textView.setText(textoInterpretado);

        return root;
    }
}