package com.example.regis_cat.Fragments;

import android.content.res.Resources;
import android.os.Bundle;

import androidx.core.text.HtmlCompat;
import androidx.fragment.app.Fragment;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.regis_cat.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PtzFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PtzFragment extends Fragment {



    public PtzFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_ptz, container, false);

        Resources resources = getResources();
        String texto = resources.getString(R.string.large_text_ptz);
        CharSequence textoInterpretado = HtmlCompat.fromHtml(texto, Html.FROM_HTML_MODE_LEGACY);

        TextView textView = (TextView) root.findViewById(R.id.txt_ptz_scrollable);
        textView.setText(textoInterpretado);

        return root;
    }
}