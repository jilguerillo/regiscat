package com.example.regis_cat.Modelo;

public class Target {
    public String NSP, TIPOCAMARA, UB_REFERENCIAL, LATITUDE, LONGITUDE, COMUNA, REGION;

    public Target(String NSP, String TIPOCAMARA, String UB_REFERENCIAL, String LATITUDE, String LONGITUDE, String COMUNA) {
        this.NSP = NSP;
        this.TIPOCAMARA = TIPOCAMARA;
        this.UB_REFERENCIAL = UB_REFERENCIAL;
        this.LATITUDE = LATITUDE;
        this.LONGITUDE = LONGITUDE;
        this.COMUNA = COMUNA;
    }

    public Target(String NSP, String TIPOCAMARA, String UB_REFERENCIAL, String LATITUDE, String LONGITUDE, String COMUNA, String REGION) {
        this.NSP = NSP;
        this.TIPOCAMARA = TIPOCAMARA;
        this.UB_REFERENCIAL = UB_REFERENCIAL;
        this.LATITUDE = LATITUDE;
        this.LONGITUDE = LONGITUDE;
        this.COMUNA = COMUNA;
        this.REGION = REGION;
    }

    public String getREGION() {
        return REGION;
    }

    public void setREGION(String REGION) {
        this.REGION = REGION;
    }

    public String getNSP() {
        return NSP;
    }

    public String getTIPOCAMARA() {
        return TIPOCAMARA;
    }

    public String getUB_REFERENCIAL() {
        return UB_REFERENCIAL;
    }

    public String getLATITUDE() {
        return LATITUDE;
    }

    public String getLONGITUDE() {
        return LONGITUDE;
    }

    public String getCOMUNA() {
        return COMUNA;
    }

    public void setNSP(String NSP) {
        this.NSP = NSP;
    }

    public void setTIPOCAMARA(String TIPOCAMARA) {
        this.TIPOCAMARA = TIPOCAMARA;
    }

    public void setUB_REFERENCIAL(String UB_REFERENCIAL) {
        this.UB_REFERENCIAL = UB_REFERENCIAL;
    }

    public void setLATITUDE(String LATITUDE) {
        this.LATITUDE = LATITUDE;
    }

    public void setLONGITUDE(String LONGITUDE) {
        this.LONGITUDE = LONGITUDE;
    }

    public void setCOMUNA(String COMUNA) {
        this.COMUNA = COMUNA;
    }
}
